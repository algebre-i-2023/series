# Séries

Les séries compilées sont téléchargeables [ici](https://gitlab.unige.ch/api/v4/projects/9130/jobs/artifacts/main/download?job=build) et peuvent être consultées [ici](https://gitlab.unige.ch/algebre-i-2023/series/-/jobs/artifacts/main/browse?job=build)